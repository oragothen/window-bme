# encoding: utf-8

import bme280
import mysql.connector as mariadb
from datetime import datetime, timedelta
import time

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import matplotlib.dates as mdates


from git import Repo
path_of_repo = "./.git"
commit_msg = "Write data"


def write_to_db(temp, pres, hum):

    print("Writing into databank...")

    mariadb_connection = mariadb.connect(user='data_input', password='bme',
                                                 database='bme')
    cursor = mariadb_connection.cursor()

    cursor.execute("""INSERT INTO fenster (temperature, pressure, humidity)
                                           VALUES ({}, {}, {});""".format(temp, pres, hum
                                           ))

    mariadb_connection.commit()

    mariadb_connection.close()

    print("Complete!")

def select_last():

    print("Getting data from databank...")

    mariadb_connection = mariadb.connect(user='data_input', password='bme',
                                                 database='bme')
    cursor = mariadb_connection.cursor()

    date = datetime.today() - timedelta(days=7)
    date_str = date.strftime('%Y-%m-%d %H:%M:%S')
    select_str = "SELECT date,temperature,pressure,humidity FROM fenster WHERE date >= \"%s\";"%date_str
    cursor.execute(select_str)

    result = cursor.fetchall()

    mariadb_connection.commit()

    mariadb_connection.close()

    print("Complete!")

    return result

def plot(data):
    dates = []
    temps = []
    presss = []
    hums = []

    for date, temp, press, hum in data:
        dates.append(date2num(date))
        temps.append(temp)
        presss.append(press)
        hums.append(hum)

    fig, axes = plt.subplots(3, sharex=True, figsize = (6.40,14.40))
    (tax, pax, hax) = axes
    
    for ax in axes:
        ax.xaxis.set_major_locator(mdates.DayLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%a"))
        plt.setp(ax.get_xticklabels(),visible=True)


    #plt.set_minor_locator()
    tax.set_ylabel("Temperature [deg C]")
    tax.plot_date(dates,temps,  xdate=True, marker = "", linestyle="-")
    #plt.savefig("public/temps.png")
    pax.set_ylabel("Pressure [hPa]")
    pax.plot_date(dates,presss, xdate=True, marker = "", linestyle="-")
    #plt.savefig("public/presss.png")
    hax.set_ylabel("Humidity [%]")
    hax.plot_date(dates,hums,   xdate=True, marker = "", linestyle="-")
    #plt.savefig("public/hums.png")
    plt.xkcd()
    plt.savefig("public/graph.png")
    size = fig.get_size_inches()*fig.dpi
    print("Plotting data complete")

    return size


def push():

    try:
        repo = Repo(path_of_repo)
        repo.git.add(update=True)
        repo.index.commit(commit_msg)
        origin = repo.remote(name='origin')
        origin.push()
        print("Successfully pushed data")
    except Exception as err:
        print('Some error occured while pushing the data')
        print(err)

    


def read_out():
    print("Reading out sensor...")
    temp, press, hum = bme280.readBME280All()
    print("Complete!")
    write_to_db(temp, press, hum)

    width, height = plot(select_last())
    txt = """<!DOCTYPE html>
    <html>
    <body>

    <h2>Data</h2>
    <p>Temperature: %f</p>
    <p>Pressure: %f</p>
    <p>Humidity: %f</p>

    <img src="graph.png" alt="Data" width="%i" height="%i">

    </body>
    </html>"""%(temp, press, hum, width, height)

    f = open("public/index.html", "w")
    f.write(txt)
    f.close()

def main():
    read_out()
    push()

if __name__ == "__main__":
    main()

